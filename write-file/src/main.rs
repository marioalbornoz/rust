use std::fs::File;

use std::io::prelude::*;


fn main() {
    let mut file = File::create("info.txt").expect("Can't open file!");
    
    file.write_all(b"Hola Rust!")
        .expect("Oops! can not read the file...");
}