fn main() {
    let _x = 1;
    let _y = 2.0;
    let _mario = "Esto es un string";
    let _boolean = true;
    
    let mate_dinamica = 8*8;
    
    let _my_array = [1,8,5,9,3,2,5];
    let _my_tupple = ([1,2,4],_mario,_x,_y,[8,9,6],[9,3,1],_boolean);
    let _my_second_tupple = (1.0, "Harry", false);

    let (_edad, _nombre_perro, _libre) = _my_second_tupple;
    
    println!("The value of x is: {}", mate_dinamica);
    println!("El diccionario : {}",_my_tupple.1);
    sum(10,10);
}

fn sum(x: i8, y:i8){
    println!("El resultado de la primera funcion es :{}", x+y);
}

