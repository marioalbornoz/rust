enum Direction {
    Up,
    Down,
    Left,
    Right
}

fn main() {
    let player_direction:Direction = Direction::Down;

    match player_direction {
        Direction::Up => println!("Estamos en la cabecera up!"),
        Direction::Down => println!("Estamos bajando con down!"),
        Direction::Left => println!("Left is right!"),
        Direction::Right => println!("Moving toward the right!"),
    }
}
