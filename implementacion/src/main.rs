struct Rectangle {
    width: u32,
    height: u32
}

impl Rectangle {
    fn print_description(&self){
        println!("Rectangle : {} x {}", self.width, self.height);
    }
    fn is_square(&self) -> bool {
        self.width == self.height
    }
}

fn main() {

    let my_rectang = Rectangle { width: 10, height: 5 };

    my_rectang.print_description();
    println!("Rectangle is a square: {}", my_rectang.is_square());

}