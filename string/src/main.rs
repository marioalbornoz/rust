struct Person {
    name: String,
    age: u8
}

impl ToString for Person {
    fn to_string(&self) -> String {
        return format!("Your name is {} and she has {} years old", self.name, self.age);
    }
}

fn main() {

    let mut my_string = String::from("How's it going? My name is Mario. ");
    println!("{}", my_string);

    println!("Lenght: {}", my_string.len());
    println!("String is empty?: {}", my_string.is_empty());

    for token in my_string.split_whitespace(){
        println!("{}", token);
    }

    println!("Does the string contain 'Mario'? :{}", my_string.contains("Mario"));

    my_string.push_str("Nice to meet you."); // String::from();
    println!("{}", my_string);

    let s = String::from("hello");
    take_ownership(s);
    let x = 5;
    makes_copy(x);


    let dani = Person { name : String::from("Daniela"), age: 20 };
    println!("{}", dani.to_string());

}

fn take_ownership(some_string: String) {
    println!("{}", some_string);
}
fn makes_copy(some_integer: i32){
    println!("{}", some_integer);
}
