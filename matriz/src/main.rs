use rand::Rng;

fn main() {
    let random_bytes = rand::thread_rng().gen::<[[u8; 8];8]>();
    println!("{:?}", random_bytes);
}