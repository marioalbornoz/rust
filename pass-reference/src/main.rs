struct Color {
    red: u8,
    green: u8,
    blue: u8
}



fn main() {
    let mut bg = Color{red: 255, green: 75, blue: 16 };
   
    print_color(&bg);

}


fn print_color(c: &Color){
    println!("Color - red:{} - green:{} - blue: {}", c.red, c.green, c.blue)
}