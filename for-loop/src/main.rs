fn main() {
    
    // for i in 1..11 {
    //     println!("The number is {}" , i);
    // }

    // let number = 1..25;
    // for i in number {
    //     println!("The number is {}", i);
    // }



    let animals = vec!["Dog", "Cat", "Rabbit"];

    for (index, animal) in animals.iter().enumerate() {
        println!("The index is {} and animal name is {}",index, animal)
    }
}
